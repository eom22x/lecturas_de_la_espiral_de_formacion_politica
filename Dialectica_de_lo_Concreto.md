# Dialéctica de la Totalidad Concreta

## El mundo de la pseudoconcreción y su destrucción

Partiremos nuestro análisis de la lectura desde los siguientes enfoques

- Observaciones generales

- Filosofía

- Práctica política

### Observaciones generales


La lectura es una propuesta desde el modelo marxista, es decir toma categorias de esta teoría y hace una interpretación, con ello haciendo un aporte al desarrollo de esta teoría. Toca diferentes ambitos filosóficos por decirlo de alguna manera, a saber, realidad, totalidad, comprensión, humanización, etc. 


### Aspectos bibliográficos de la obra

El texto se encuentra en la obra **Dialéctica de lo Concreto** de la editorial Grijalbo, apoyada por el filosofo español-mexicano Adolfo Sanchez Vazquez como lo hace notar este al prologar la edición. Hago notar esto porque es una relación politica entre estos dos filosofos y es evidente que hay una intención para apoyar su edición, por eso hago esta asociación. Pero el autor tambien es resultado del tiempo en el que vive, entoces vamonos a los aspectos biográficos de nuestros personajes
